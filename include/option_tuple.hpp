#ifndef MOTL_OPTION_TUPLE_HPP
#define MOTL_OPTION_TUPLE_HPP

/**
* mostly non-owning tuple of option long and short name,
* and help text.
* In the simplest of cases (single language support)
**/

#include <string_view>
#include <array>
#include <algorithm>

namespace motl {

using short_name_type = std::array<char, 4>;

struct option_tuple
{
    //! the "--" is implicit
    std::string_view long_name;
    std::string_view help_text;
    // four bytes to allow for utf-8;
    // to limit unnecessary padding,
    //! the "-" is implicit
    short_name_type short_name;
};

constexpr short_name_type no_short_name() {
    return short_name_type{{'\0','\0','\0','\0'}};
}

constexpr std::size_t short_name_len(short_name_type const & short_name)
{
    // std::memchr works in void * and int and is not constexpr
    // std::strlen requires a null byte and has no bound limit and is not constexpr
    // std::find is not constexpr until c++20
    // std::count_if is not constexpr until c++20
    // but, this is so short, we can not only unroll the loop, but also dispence with all branches
    // just make sure we update it if the size ever changes
    static_assert(
        std::tuple_size<
            typename std::remove_cv<
                typename std::remove_reference<decltype(short_name)>::type
            >::type
        >::value == 4,
        "review short name length implementation"
    );

    return static_cast<unsigned>(short_name[0] != '\0')
    + static_cast<unsigned>((short_name[0] != '\0') & (short_name[1] != '\0'))
    + static_cast<unsigned>((short_name[0] != '\0') & (short_name[1] != '\0') & (short_name[2] != '\0'))
    + static_cast<unsigned>((short_name[0] != '\0') & (short_name[1] != '\0') & (short_name[2] != '\0') & (short_name[3] != '\0'))
    ;
}

constexpr unsigned expected_byte_length(char first_byte)
{
    constexpr unsigned char const byte_leads[] = {0x00 ,0xC0u, 0xE0, 0xF0};
    constexpr unsigned char const byte_masks[] = {0x80 ,0xE0u, 0xF0, 0xF8};
    return static_cast<unsigned>((byte_masks[0] & static_cast<unsigned char>(first_byte)) == byte_leads[0])
        + 2u*static_cast<unsigned>((byte_masks[1] & static_cast<unsigned char>(first_byte)) == byte_leads[1])
        + 3u*static_cast<unsigned>((byte_masks[2] & static_cast<unsigned char>(first_byte)) == byte_leads[2])
        + 4u*static_cast<unsigned>((byte_masks[3] & static_cast<unsigned char>(first_byte)) == byte_leads[3])
    ;
}

// facilitates simple comparison for use with algorithms
constexpr bool operator==(option_tuple const & lhs, option_tuple const & rhs) noexcept
{
    return (lhs.long_name == rhs.long_name)
    && (lhs.short_name ==rhs.short_name)
    ;
}

constexpr bool operator!=(option_tuple const & lhs, option_tuple const & rhs) noexcept
{
    return !(lhs == rhs);
}

// facilitates simple comparison for use with ordered containers

constexpr bool operator<(option_tuple const & lhs, option_tuple const & rhs) noexcept
{
    return (lhs.long_name < rhs.long_name)
    || ((lhs.long_name == rhs.long_name) && (lhs.short_name < rhs.short_name));
}

constexpr bool operator>(option_tuple const & lhs, option_tuple const & rhs) noexcept
{
    return rhs < lhs;
}

constexpr bool operator<=(option_tuple const & lhs, option_tuple const & rhs) noexcept
{
    return !(rhs < lhs);
}

constexpr bool operator>=(option_tuple const & lhs, option_tuple const & rhs) noexcept
{
    return !(lhs < rhs);
}


} // namespace motl

#endif // MOTL_OPTION_TUPLE_HPP
