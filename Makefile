#!/usr/bin/make -f

# GNU docs say /usr/local but linux filesystem heirarchy
# says it is for the local administrator.
prefix ?= /usr

headerdir ?= $(prefix)/include

installdir ?= $(headerdir)/motl

srcdir ?= ./include

all : 

clean :

headers = $(shell find "$(srcdir)")

.PHONY : headers

uninstall : 
	rm -rf "$(DESTDIR)$(installdir)"

install : headers
	mkdir -p "$(DESTDIR)$(installdir)"
	cp -r $(srcdir)/. "$(DESTDIR)$(installdir)"
