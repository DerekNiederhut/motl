#ifndef MOTL_PARSE_HPP
#define MOTL_PARSE_HPP

#include "motl/tokenized_option_group.hpp"
#include "motl/option_tuple.hpp"
#include <charconv>
#include <optional>
#include <string_view>
#include <limits>
#include <system_error>
#include <type_traits>

namespace motl {

template<typename T>
std::optional<T> impl_parse_int_magnitude(std::string_view token, int base)
{
    T val{};
    // try to convert
    std::from_chars_result const status = std::from_chars(
        token.data(),
        token.data() + token.size(),
        val,
        base
    );
    // if that conversion succeeded and used all characters, we can return the value;
    // otherwise, return empty; let the caller try with another base
    if (status.ec == std::errc{} && status.ptr == token.data() + token.size())
    {
        return val;
    }
    return std::nullopt;
}

template<typename T>
typename std::enable_if<
    std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer,
    std::optional<T>
>::type parse_as(std::string_view original_token)
{
    // from_chars is fast, and takes a string_view, but does not detect base automatically,
    //   does not allow 0x prefixes on hex bases, does not handle + sign (only - for signed),
    //   (also does not ignore whitespace but we should have already stripped that)
    // strto* can "detect" bases, but requires a null-terminated string, which is silly,
    //   and its "detection" method is fairly primitive and reproducible
    bool should_negate = false;
    int base_guess = 10;
    // so first, strip any leading '+' or'-' sign
    if (original_token[0] == '-')
    {
        should_negate = true;
        original_token.remove_prefix(1);
        if (!std::numeric_limits<T>::is_signed | original_token.empty())
        {
            return std::nullopt;
        }
    }
    else if (original_token[0] == '+')
    {
        original_token.remove_prefix(1);
        if (original_token.empty())
        {
            return std::nullopt;
        }
    }

    // we might guess wrong on the base, so make a copy of the string_view so we
    // can go back
    std::string_view token = original_token;
    // now for "base detection"
    if ((token.size() > 1) & (token[0] == '0'))
    {
        if ((token[1] == 'X') | (token[1] == 'x'))
        {
            base_guess = 16;
            token.remove_prefix(2);
        }
        else if ((token[1] == 'B') | (token[1] == 'b'))
        {
            base_guess = 2;
            token.remove_prefix(2);
        }
        else
        {
            base_guess = 8;
            token.remove_prefix(1);
        }
    }

    static_assert(std::is_default_constructible<T>::value, "Parser implementation requires that integer type be default-cosntructible.");

    std::optional<T> parse_attempt = impl_parse_int_magnitude<T>(token, base_guess);
    // if that conversion succeeded and used all characters, we can return immediately
    if (static_cast<bool>(parse_attempt))
    {
        return std::optional<T> (should_negate ? -*parse_attempt : *parse_attempt);
    }

    // if we failed, revert prefix removal
    token = original_token;
    switch (base_guess)
    {
    case 8:
        // maybe leading zero was just a zero; try as base 10
        parse_attempt = impl_parse_int_magnitude<T>(token, 10);
        if (static_cast<bool>(parse_attempt))
        {
            return std::optional<T> (should_negate ? -*parse_attempt : *parse_attempt);
        }
        [[fallthrough]];
    case 2:
        // we would only guess 2 if we saw a 'b', so it cannot be base 10,
        // but could be base 16
        [[fallthrough]];
    case 10:
        // we may have assumed 10 but it is actually 16
        parse_attempt = impl_parse_int_magnitude<T>(token, 16);
        if (static_cast<bool>(parse_attempt))
        {
            return std::optional<T> (should_negate ? -*parse_attempt : *parse_attempt);
        }
        // from_chars can go all the way from base 2 to base36,
        // but if we saw a b, we're at least base 12,
        // and in practice, we're going to see 2,8,10,16, 32, or 64,
        // but from_chars won't do 64, so we'll give 32 a try and then give up
        parse_attempt = impl_parse_int_magnitude<T>(token, 32);
        if (static_cast<bool>(parse_attempt))
        {
            return std::optional<T> (should_negate ? -*parse_attempt : *parse_attempt);
        }
        [[fallthrough]];
    default:
        break;
    }

    return std::nullopt;
}

template<typename T, typename Tokenized_Group>
typename std::enable_if<
    std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer,
    std::optional<T>
>::type find_as(Tokenized_Group const & grp, option_tuple const & opt)
{
    std::optional<std::string_view> const maybe_token = grp.find(opt);
    if (!static_cast<bool>(maybe_token) || maybe_token->empty())
    {
        return std::nullopt;
    }
    return parse_as<T>(*maybe_token);
}

template<typename T, typename Tokenized_Group>
typename std::enable_if<
    std::numeric_limits<T>::is_specialized && std::numeric_limits<T>::is_integer,
    std::optional<T>
>::type find_as(Tokenized_Group const & grp, std::string_view long_name)
{
    std::optional<std::string_view> const maybe_token = grp.find(long_name);
    if (!static_cast<bool>(maybe_token) || maybe_token->empty())
    {
        return std::nullopt;
    }
    return parse_as<T>(*maybe_token);
}

} // namespace motl

#endif // MOTL_PARSE_HPP
