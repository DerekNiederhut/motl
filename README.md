# Minimal Option Tokenizer Library

For tokenizing (and maybe parsing) command line options, with a focus on a minimal, but still useful, implementation.

In particular, this means:
- no external dependencies outside the standard library.
- header-only (no separate compilation / installation)
- limited memory usage & allocation (prefer std::string_view to copying std::strings, allow user to supply their own containers to avoid the overhead of, for example, std::map)
- minimal localization support: 
    - uses narrow characters, compares bytes lexicographically without normalizing code points or comparing for equivalence
    - simple estimates on glyph count (one per code point) that assume valid UTF-8 (or the ascii subset); only used for streaming
    - numeric conversions are not localized, just a wrapper around std::from_chars to allow things like "0x" prefixes and to try to guess at the base (2, 10, 16, or 32), without having to make null-terminated copies for example to use std::strtoll
- minimal complexity via minimal features; 
    - one value (or no value) per option (repeated options not supported)
    - data value/type validation (beyond some simple numeric parsing) left to client code
    - default values left to client code
    - all options must have a name (no positional options)
    - all options must use either the "--long-option-name[=val]" or the "-o[=][val]" short option syntax
- minimal license encumbrance
