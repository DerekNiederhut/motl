#ifndef MOTL_PARSED_OPTION_GROUP_HPP
#define MOTL_PARSED_OPTION_GROUP_HPP

#include "motl/option_tuple.hpp"

#include <utility>
#include <algorithm>
#include <cctype>
#include <optional>
#include <cstring>

namespace motl {

inline std::string_view trim_both_ends(std::string_view original, std::string_view trim_chars)
{
    std::string_view::size_type const first_valid = original.find_first_not_of(trim_chars);
    if (first_valid == std::string_view::npos)
    { return std::string_view{};}
    // if we found something valid from the front, we'll find it from the back,
    // even if it is at the same position
    // we want the length of the substring, but to get the subtraction to work right,
    // we need end - begin, not last - begin, so add 1 to correct
    return original.substr(first_valid, 1u + original.find_last_not_of(trim_chars) - first_valid);
}

template<typename Itr>
Itr find_long_name_match(Itr opt_begin, Itr opt_end, std::string_view long_name)
{
    return std::find_if(
        opt_begin,
        opt_end,
        [=](option_tuple const & opt){return opt.long_name == long_name;}
    );
}



template<typename Itr>
Itr find_short_name_match(Itr opt_begin, Itr opt_end, std::string_view arg_name)
{
    return std::find_if(
        opt_begin,
        opt_end,
        [=](option_tuple const & opt){
            return 0 == std::lexicographical_compare(
                std::cbegin(opt.short_name),
                std::cbegin(opt.short_name) + short_name_len(opt.short_name),
                arg_name.cbegin(),
                arg_name.cend()
            );
        }
    );
}

constexpr std::string_view space_equal_or_quote_chars()
{
    return std::string_view(" \t\v\f\r\n=\"'`");
}

constexpr std::string_view equal_or_quote_chars()
{
    // could also use std::find_if_not() with std::isspace()
    // to get iterators, but that wouldn't be constexpr until c++20,
    // so we'll hope the compiler combines string literals instead
    return std::string_view("=\"'`");
}

constexpr std::string_view whitespace_chars()
{
    return std::string_view(" \t\v\f\r\n");
}

enum parse_state : unsigned char
{
    searching_for_argument_name,
    searching_for_argument_value
};

struct parse_context
{

    constexpr parse_context() noexcept
    : current_state(parse_state::searching_for_argument_name)
    , pending_tuple{
        std::string_view{},
        std::string_view{},
        no_short_name()
    }
    {}

    parse_state current_state;
    option_tuple pending_tuple;
};

//! Tempalted on container type with no default;
//! Given that most programs have a somewhat small (say, at most dozens)
//! of options, if your CPU has a much faster cache than RAM,
//! you probably want to be using some wrapper around a sorted vector;
//! boost::container::flat_map would be an excellent candidate,
//! but including that adds a dependency outside the standard library and
//! requires including headers that not everyone might need or want;
//! other implementations of contiguous/semi-contiguous sorted associative containers exist.
//! If you don't care at all about performance, you could use std::map.
template< template<typename ...> typename Associative_Container_Type, typename ... Args>
class tokenized_option_group
{
    public:
        using underlying_container = Associative_Container_Type<option_tuple, std::string_view, Args... >;
        using const_iterator = typename underlying_container::const_iterator;
        using value_type = typename underlying_container::value_type;

        constexpr tokenized_option_group() = default;

        //! tokenize main arguments that match an option found within the iterator range;
        //! iterators must point to option_tuples, may be const_iterators, such as
        //! from option_group.cbegin(), option_group.cend()
        template<typename Itr>
        tokenized_option_group(int argc, char const * argv[], Itr opt_begin, Itr opt_end)
        {

            // argv[0] is the program name or path or command, it need not be checked for an option
            if (argc < 1)
            {
                return;
            }


            parse_context context{};
            // argv[0] is the program name or path or command, it need not be checked for an option
            for (int idx = 1; idx < argc; ++idx)
            {
                // make a string_view of the current argument
                std::string_view arg_view(argv[idx]);
                arg_view = trim_both_ends(arg_view, whitespace_chars());

                if (arg_view.size() == 0)
                {
                    continue;
                }

                // does it start with a hyphen?
                if ( arg_view[0] == '-')
                {
                    // where might it end?
                    std::string_view::size_type delimiter = arg_view.find_first_of(space_equal_or_quote_chars());
                    Itr search_itr = opt_end;
                    // does it have a second hyphen after that one?
                    if (arg_view.size() > 1 && arg_view[1] == '-')
                    {
                        // is it among the permitted options?
                        search_itr = find_long_name_match(
                            opt_begin,
                            opt_end,
                            // long name; if delimiter was not found, delimiter gives npos,
                            // but if it was found, we want to subtract 2, because substr
                            // uses count, not end, like iterators do.
                            // realistically, string_view::size_type is going to be at least
                            // 16 bits signed on any platform, and no realistic command option is going to be
                            // 32767 bytes long; even if they're all UTF-8 as 4 bytes, that'd be 8191 code points.
                            // further, substr already checks the bounds and does not overrun,
                            // so we can subtract 2 (the starting pos) unconditionally without worry;
                            // if someone wants a --long-name option that is 8191 code poitns long
                            // badly enough, they can write their own parser.
                            arg_view.substr(2, delimiter - 2)
                        );

                    }
                    else // short name
                    {
                        search_itr = find_short_name_match(
                            opt_begin,
                            opt_end,
                            // not bothering with bounds check because substr will check for us
                            arg_view.substr(1, expected_byte_length(arg_view[1]))
                        );
                        // if we didn't find a delimiter, assume the next byte following this code point
                        // might be a value
                        if (delimiter > arg_view.size())
                        {
                            // one extra for the '-'
                            delimiter = 1 + expected_byte_length(arg_view[1]);
                        }

                    }
                    // found a match
                    if (search_itr != opt_end)
                    {
                        // were we looking for a value previously, and found a command instead?
                        // if so, add it as an option with no value before overwriting
                        if (context.current_state == searching_for_argument_value)
                        {
                            mapping.emplace(context.pending_tuple, std::string_view());
                        }
                        context.current_state = searching_for_argument_value;
                        context.pending_tuple = *search_itr;
                        // skip command
                        // substr requires pos to be within range, or it throws an excpetion
                        if (delimiter > arg_view.size())
                        {
                            arg_view = std::string_view();
                        }
                        else
                        {
                            arg_view = arg_view.substr(delimiter, std::string_view::npos);
                        }
                    }
                }

                // strip any quotes or whitespace
                arg_view = trim_both_ends(arg_view, space_equal_or_quote_chars());
                // either we didn't find a command, or we did and have processed it,
                // removing it from the substring.
                // But, it is possible that we previously found a value and now are finding another value.
                if (  (context.current_state == searching_for_argument_name)
                    | (arg_view.size() == 0)
                ) {
                    continue;
                }

                // otherwise, use the remaining value with pending option
                mapping.emplace(context.pending_tuple, arg_view);

            }

            // if there is a name but not value when the loop is finished, assume it has no value
            if (context.current_state == searching_for_argument_value)
            {
                mapping.emplace(context.pending_tuple, std::string_view());
            }
        }

        //! allow read-only iterator access to the underlying container
        const_iterator cbegin() const
        {
            return mapping.cbegin();
        }

        const_iterator cend() const
        {
            return mapping.cend();
        }


        //! note that although string_view has an empty value state,
        //! we need to differentiate between
        //!     "option with no value was provided"
        //! and "option with no value was not provided",
        //! so we cannot simply return an empty string_view.
        //! lookup by long name
        std::optional<std::string_view> find(std::string_view long_name) const
        {
            return entry_or_empty(
                std::find_if(
                    mapping.cbegin(),
                    mapping.cend(),
                    [long_name](value_type const & candidate){return candidate.first.long_name == long_name;}
                )
            );
        }
        //! lookup by short name
        std::optional<std::string_view> find(short_name_type const & short_name) const
        {
            return entry_or_empty(std::find_if(
                     mapping.cbegin(),
                     mapping.cend(),
                     [short_name](value_type const & candidate){return candidate.first.short_name == short_name;}
                 )
            );
        }
        //! lookup either
        std::optional<std::string_view> find(option_tuple const & needle) const
        {
            return entry_or_empty(std::find_if(
                     mapping.cbegin(),
                     mapping.cend(),
                     [needle](value_type const & candidate){
                        return (candidate.first.short_name == needle.short_name)
                            || (candidate.first.long_name == needle.long_name);
                    }
                 )
            );
        }


    private:

        std::optional<std::string_view> entry_or_empty(const_iterator itr) const
        {
            return (itr == mapping.cend()) ?
                std::optional<std::string_view>(std::nullopt)
                :
                std::optional<std::string_view>(itr->second)
            ;
        }

        underlying_container mapping;
};

} // namespace motl

#endif // MOTL_PARSED_OPTION_GROUP_HPP
