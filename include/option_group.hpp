#ifndef MOTL_OPTION_GROUP_HPP
#define MOTL_OPTION_GROUP_HPP

/**
* class for containing possible options.
* supports constexpr construction for options hard-coded into a program,
* but can also be populated at runtime
* for example to support values loaded for example from a translation file
**/

#include "motl/option_tuple.hpp"

#include <string>
#include <string_view>
#include <vector>
#include <utility>
#include <type_traits>
#include <iterator>
#include <initializer_list>

#include <ostream>
#include <iomanip>
#include <cctype>
#include <algorithm>

namespace motl {


template<typename T, typename Enable = void>
struct has_iterator_insert : public std::false_type
{};

template<typename T>
struct has_iterator_insert<
    T,
    typename std::enable_if<
        std::is_same<
            void,
            std::void_t<
                decltype(std::declval<T>().insert(
                    std::declval<typename T::const_iterator>(),
                    std::declval<option_tuple>())
                )
            >
        >::value
    >::type
>
: public std::true_type
{};

template<typename T, typename Enable = void>
struct has_value_insert : public std::false_type
{};

template<typename T>
struct has_value_insert<
    T,
    typename std::enable_if<
        std::is_same<
            void,
            std::void_t<
                decltype(std::declval<T>().insert(std::declval<option_tuple>()))
            >
        >::value
    >::type
>
: public std::true_type
{};

// yes, a default of vector means that even applications that want
// another type have to include the vector header, but vector is
// so often an excellent choice that there's a good chance you were going
// to include it anyway.
template< typename Underlying_Container = std::vector<option_tuple > >
class option_group
{
    public:

        using container_type = Underlying_Container;
        using const_iterator = typename container_type::const_iterator;

        constexpr option_group() = default;

        constexpr option_group(std::initializer_list<option_tuple> il)
        : options(il)
        {}

        constexpr option_group(container_type && val)
        : options(std::move(val))
        {}

        template<typename ... Args>
        explicit constexpr option_group(Args && ... args)
        : options(std::forward<Args>(args)...)
        {}

        // covers containers that sort themselves and can insert with just a value_type
        constexpr option_group & add(option_tuple && v)
        {
            if constexpr (has_value_insert<Underlying_Container>::value )
            {
                options.insert(std::move(v));
            }
            else
            {
                static_assert(has_iterator_insert<Underlying_Container>::value, "must be able to insert using iterator or value.");
                options.insert(options.end(), std::move(v));
            }
            return *this;
        }

        // permit readonly access
        constexpr const_iterator cbegin() const
        {
            return options.cbegin();
        }

        constexpr const_iterator cend() const
        {
            return options.cend();
        }

    private:
        container_type options;
};

//! approximate for multiple reasons:
//! first, the UTF-8 is assumed to be valid,
//! and thus checking the leading byte is sufficient to know byte length;
//! second, assumes all code points are printable glpyhs (except ascii, which is checked);
//! finally, assumes all glyphs take up equivalent space (which is already incorrect even in ascii thanks to tabs).
//! Though this is incorrect it is probably acceptably close in this use case.
//! Most command-line help strings are meant to consist of printable characters,
//! and it is the programmer's repsonsibility to either hard-code or provide a translation
//! file with valid UTF-8 (arbitrary byte streams are not expected).
//! If one line is a little longer than the others, it probably still looks "good enough"
//! that the cost of linking in the ICU library is not warranted.
std::size_t approximate_display_length(std::string_view v)
{
    std::size_t printable_ascii = 0;
    std::size_t multibyte_points = 0;

    for (auto itr = v.cbegin(); itr != v.cend(); ++itr)
    {
        unsigned expected_bytes = expected_byte_length(*itr);
        // the int returned by isprint is just "nonzero", not "1",
        // so make it a bool before casting to unsigned to avoid crazy long spacing
        printable_ascii += (1u == expected_bytes) ? static_cast<unsigned>(0 != std::isprint(*itr)) : 0u;
        multibyte_points += static_cast<unsigned>(expected_bytes > 1u);
        // skip the remaining bytes
        itr += (expected_bytes > 1u) ? 0u : expected_bytes - 1u;
    }

    return printable_ascii + multibyte_points;
}

template< typename Underlying_Container>
std::ostream & operator<<(std::ostream & strm, option_group<Underlying_Container> const & c)
{
    std::size_t max_long_name_len = 0;

    for (auto itr = c.cbegin(); itr != c.cend(); ++itr)
    {
        max_long_name_len = (std::max)(max_long_name_len, approximate_display_length(itr->long_name));
    }

    auto const previous_fill = strm.fill();
    strm.fill(' ');
    // reserve a little space and allow for unconditional subtraction in the loop
    max_long_name_len = (std::max)(max_long_name_len, std::size_t{2u});

    for (auto itr = c.cbegin(); itr != c.cend(); ++itr)
    {
        strm << "    ";
        if (itr->short_name[0] != '\0')
        {
            strm << '-';
            for (std::size_t idx = 0; idx < std::size(itr->short_name); ++idx)
            {
                strm << itr->short_name[idx];
            }
            if (!(itr->long_name.empty()))
            {
                strm << ',';
            }
        }
        strm << '\t';

        if (!(itr->long_name.empty()))
        {
            strm << '-';
            strm << '-';
            strm << std::left << std::setw(max_long_name_len-2u) ;
            strm << itr->long_name;
        }
        strm << '\t';
        strm << itr->help_text;
        strm << '\n';
    }
    strm.fill(previous_fill);
    return strm;
}

} // namespace motl

#endif // MOTL_OPTION_GROUP_HPP
